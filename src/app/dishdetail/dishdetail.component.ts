import { Component, OnInit , Inject } from '@angular/core';
import { Params, ActivatedRoute} from '@angular/router';
import {Location } from '@angular/common';

import {Dish} from '../shared/dish';
import { DishService } from '../services/dish.service';
import { Comment} from '../shared/comment';

import 'rxjs/add/operator/switchMap';

import { FormBuilder, FormGroup, Validators }  from '@angular/forms';
import { baseURL } from '../shared/baseurl';

import { trigger, state, style, animate, transition } from '@angular/animations';
import { visibility, flyInOut,expand } from '../animations/app.animation';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
    },  
  animations: [
    visibility(),
    flyInOut(),
    expand()  
  ]
})
export class DishdetailComponent implements OnInit {
  dish : Dish;
  dishcopy = null;
  dishIds : number[];
  prev : number;
  next : number;
  commentForm : FormGroup;
  formErrors = {
    'author': '',  
    'comment': ''    
  };
  comment : Comment; 
  errMess: string;
  visibility = 'shown';
  validationMessages = {
    'author': {
      'required':      'Author Name is required.',
      'minlength':     'Author Name must be at least 2 characters long.'
    },
    'comment':{
      'required':      'Comment is required.'      
    }
  };

  constructor(private dishservice : DishService, 
    private route : ActivatedRoute,
    private ocation : Location,
    private fb: FormBuilder,
  @Inject('BaseURL') private BaseURL ) { 
      this.createForm();
  }

  ngOnInit() {
     this.dishservice.getDishIds().subscribe(dishIds => this.dishIds = dishIds);
     this.route.params
     .switchMap((params: Params) => { this.visibility = 'hidden'; return this.dishservice.getDish(+params['id']); })
     .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
         errmess => { this.dish = null; this.errMess = <any>errmess; });
  }

  setPrevNext(dishId : number){
    let index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }

  goBack(): void{
    console.log("back");
    //this.location.back;
  }

  createForm()
  {
   this.commentForm = this.fb.group({
     author: ['',[Validators.required,Validators.minLength(2)]],
     rating :[5],
     comment:['',[Validators.required]]
   });

   this.commentForm.valueChanges.subscribe( data => this.onValueChanged(data));
   
   this.onValueChanged(); //(re)set validation messages
  }

  onValueChanged(data? : any){
    if(!this.commentForm) { return; }

    const form = this.commentForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onSubmit(){
    this.comment = new Comment();
    this.comment.author = this.commentForm.value.author;
    this.comment.rating = this.commentForm.value.rating;
    this.comment.comment= this.commentForm.value.comment;
    this.comment.date = Date.now().toString();
    this.dishcopy.comments.push(this.comment);
    this.dishcopy.save()
    .subscribe(dish => { this.dish = dish; console.log(this.dish); });
    
    this.commentForm.reset({
      comment : '',
      rating :5,
      author : ''
    });

  
  }
}

