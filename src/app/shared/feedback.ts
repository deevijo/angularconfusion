// this is going to be datamodel 

export class Feedback{
    firstname : string;
    lastname : string; 
    telnum : string;
    email : string;
    agree : boolean;
    contracttype: string;
    message : string;
};

export const ContactType = ['None', 'Tel', 'Email'];