import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { ProcessHttpmsgService} from './process-httpmsg.service';
import { Feedback } from "../shared/feedback";
import { Observable} from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import { baseURL } from '../shared/baseurl';


@Injectable()
export class FeedbackService {

  feedbackcopy = null;

  constructor(private restangular: Restangular, private processHttpMsgService : ProcessHttpmsgService) { }

  submitFeedback(feedback : Feedback) : Observable<any>
  {
    this.feedbackcopy = new Feedback();
    this.feedbackcopy.agree = feedback.agree;
    this.feedbackcopy.contracttype = feedback.contracttype;
    this.feedbackcopy.email = feedback.email;
    this.feedbackcopy.firstname = feedback.firstname;
    this.feedbackcopy.lastname = feedback.lastname;
    this.feedbackcopy.message = feedback.message;
    this.feedbackcopy.telnum = feedback.telnum;

    return this.restangular.all('feedback').post();
  }


}
