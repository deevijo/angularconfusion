import { Injectable } from '@angular/core';

import { Leader} from '../shared/leader';
import { LEADERS } from '../shared/leaders';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

import { RestangularModule, Restangular } from 'ngx-restangular';
import { ProcessHttpmsgService} from './process-httpmsg.service';

@Injectable()
export class LeaderService {

  constructor(private restangular: Restangular, private processHttpMsgService : ProcessHttpmsgService) { }

  getLeaders() :Observable<Leader[]>{
     return this.restangular.all('leaders').getList();
  }

  getFeaturedLeaders() : Observable<Leader>{
    return this.restangular.all('leaders').getList({featured: true})
    .map(dishes => dishes[0]);
  }

  getLeader(id : number) : Observable<Leader>{
    return  this.restangular.one('leaders',id).get();
  }
}
