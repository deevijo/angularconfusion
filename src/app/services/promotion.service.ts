import { Injectable } from '@angular/core';
import { Promotion } from '../shared/promotion';
import { PROMOTIONS } from '../shared/promotions';
import { resolve } from 'q';


import {Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

import { RestangularModule, Restangular } from 'ngx-restangular';
import { ProcessHttpmsgService} from './process-httpmsg.service';

@Injectable()
export class PromotionService {

  constructor(private restangular: Restangular, private processHttpMsgService : ProcessHttpmsgService) { }

  getPromotions() : Observable<Promotion[]>{
    return this.restangular.all('promotions').getList();
  }  

  getPromotion(id : number) : Observable<Promotion> {
    return  this.restangular.one('promotions',id).get();
  }

  getFeaturedPromotion() : Observable<Promotion >{
    return this.restangular.all('promotions').getList({featured: true})
    .map(dishes => dishes[0]);
  }

}
