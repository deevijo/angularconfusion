import { TestBed, inject } from '@angular/core/testing';

import { ProcessHttpmsgService } from './process-httpmsg.service';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';

describe('ProcessHttpmsgService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProcessHttpmsgService]
    });
  });

  it('should be created', inject([ProcessHttpmsgService], (service: ProcessHttpmsgService) => {
    expect(service).toBeTruthy();
  }));
  
});
